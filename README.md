[Not finished]

Draft smart contract with standard ERC20

The contract includes 4 classes:
  - ERC20 - an abstract class for implementing the standard.
  - SafeMath - a class for standard mathematical functions, with checking fields and results.
  - StandardToken - the basic implementation of the token, with the inclusion of classes ERC20, SafeMath.
  - BeaverTestEth contact implementation.

Main functionality of the BeaverTestEth contract

- Token purchase (buy)
- Token exchange between users (transfer)
- send bounty tokens (sendBounty)
- User Balance Check (balanceOf)
- token sales stop (halt,unHalt)
- Refund balance (refund) [ not finish ]