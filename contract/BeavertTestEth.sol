pragma solidity ^0.5.2;

contract ERC20 {
    uint256 public totalSupply;
    function balanceOf(address _owner) public view returns (uint256 balance);
    function transfer(address _to, uint256 _value) public returns (bool success);
    function transferFrom(address _from, address _to, uint256 _value) public returns (bool success);
    function approve(address spender, uint256 value) public returns (bool success);
    function allowance(address _owner, address _spender) public returns (uint256 remaining);
    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(address indexed owner, address indexed spender, uint256 value);
}

/**
 * Math operations with safety checks
 */
contract SafeMath {
   function safeMul(uint256 a, uint256 b) internal pure  returns (uint256) {
    uint256 c = a * b;
     return c;
  }

  function safeDiv(uint256 a, uint256 b) internal pure returns (uint256) {
    assert(b > 0);
    uint256 c = a / b;
    assert(a == b * c + a % b);
    return c;
  }

  function safeSub(uint256 a, uint256 b) internal pure returns (uint256) {
    assert(b <= a);
    return a - b;
  }

  function safeAdd(uint256 a, uint256 b) internal pure returns (uint256) {
    uint256 c = a + b;
    assert(c>=a && c>=b);
    return c;
  }

}

contract StandardToken is ERC20, SafeMath {

    /* Token supply got increased and a new owner received these tokens */
    event Minted(address receiver, uint256 amount);

    /* Actual balances of token holders */
    mapping(address => uint256) balances;

    /* approve() allowances */
    mapping (address => mapping (address => uint256)) allowed;

    /* Interface declaration */
    function isToken() external pure returns (bool) { return true; }

    /**
     * Reviewed:
     * - Interger overflow = OK, checked
     */
    function transfer(address _to, uint256 _value) public returns (bool) {
        if (balances[msg.sender] >= _value && balances[_to] + _value > balances[_to]) {
            balances[msg.sender] -= _value;
            balances[_to] += _value;
            emit Transfer(msg.sender, _to, _value);
            return true;
        } else {
            return false;
            }
    }

    function balanceOf(address _account) public view returns (uint256) {
        return balances[_account];
    }

    function transferFrom(address _from, address _to, uint256 _value) public returns (bool success) {
        if (balances[_from] >= _value && allowed[_from][msg.sender] >= _value && balances[_to] + _value > balances[_to]) {
            balances[_to] += _value;
            balances[_from] -= _value;
            allowed[_from][msg.sender] -= _value;
            emit Transfer(_from, _to, _value);
            return true;
        }
        else {return false;}
    }

     function approve(address _spender, uint256 _value) public returns (bool success) {
        require((_value == 0) || (allowed[msg.sender][_spender] == 0), "amount = 0 or allowed = 0");

        allowed[msg.sender][_spender] = _value;
        emit Approval(msg.sender, _spender, _value);
        return true;
    }

    function allowance(address _owner, address _spender) public returns (uint256) {
        return allowed[_owner][_spender];
    }

    /*
    * Fix for the ERC20 short address attack
    */
    modifier onlyPayloadSize(uint256 size) {
        assert(msg.data.length >= size + 4);
        _;
    }

}

contract BeaverTestEth is StandardToken {

    string public name = "Beaver Test Eth";
    string public symbol = "BTE";
    uint256 private balContr = 0;
    uint8 public decimals = 18;
    uint256 public tokens = 5000000;
    uint256 public bounty = 5000000;// Bounty count 5 000 000
    uint256 public totalTokens = 0;//10 000 000
    uint256 public totalSupply = 0; // totalEthInWei
    uint256 internal multiplier = 1000000000000000000;
    uint256 public unitsOneEthCanBuy = 10;
    uint256 public refundPercent = 70; // 70% refund

    bool public halted = false; //the founder address can set this to true to halt the crowdsale due to emergency
    address payable public owner = address(0x111122223333444455556666777788889999AAAABBBBCCCCDDDDEEEEFFFFCCCC);

    /* This generates a public event on the blockchain that will notify clients */
    event Buy(address indexed sender, uint256 eth, uint256 fbt);
    event LogRefund(address receiver, uint amount);
    event TokensSent(address indexed to, uint256 value);
    event ContributionReceived(address indexed to, uint256 value);

    constructor() public {
        owner = msg.sender;
        // Sub from total tokens bounty pool
        totalTokens = safeAdd(tokens, bounty);
        totalSupply = safeMul(totalTokens, multiplier);
        balances[owner] = totalSupply;
        emit Minted(msg.sender,totalSupply);
    }

    function curs(uint256 _value) internal view returns(uint256){
        return _value * unitsOneEthCanBuy;
    }

    /**
      * The basic entry point to participate the crowdsale process.
      *
      * Pay for funding, get invested tokens back in the sender address.
       */
    function buy() internal returns(bool) {
        //processBuy(msg.sender, msg.value);
        require(!halted, "sale was stoped");
        require(msg.value>0,"amount for buy = 0 ");

        uint256 buyTokensSupply = curs(msg.value);
        require(balances[owner] >= buyTokensSupply, "not enough tokens to buy");

        balContr += msg.value * refundPercent / 100;  // msg.value * (100 - refundPercent) / 100

        balances[msg.sender] = safeAdd(balances[msg.sender], buyTokensSupply);
        balances[owner] = safeSub(balances[owner], buyTokensSupply);

        emit Buy(msg.sender, msg.value, buyTokensSupply);
        /* Emit log events */
        emit TokensSent(msg.sender, buyTokensSupply);
        emit ContributionReceived(msg.sender, msg.value);
        emit Transfer(owner, msg.sender, buyTokensSupply);
        // Send wei to founder address
        owner.transfer(msg.value * (100 - refundPercent) / 100);
        return true;
    }

    /*
     Transfer bounty tokens to target address
    */
    function sendBounty(address _to, uint256 _value) external onlyOwner() {
        require(bounty>_value,"the requested amount should not exceed the balance");

        bounty = safeSub(bounty, _value);
        balances[_to] = safeAdd(balances[_to], safeMul(_value, multiplier));

        // Emit log events
        emit TokensSent(_to, safeMul(_value, multiplier));
        emit Transfer(owner, _to, safeMul(_value, multiplier));
    }

    /*
     Refund BTE
     */
    function refundInToken(uint256 amountRequested) external payable returns(bool) {
        require(amountRequested > 0,"requested quantity must be greater than 0");
        require(amountRequested <= balances[msg.sender],"the requested amount should not exceed the balance");

        uint256 ethRequested = amountRequested / unitsOneEthCanBuy;

        balances[msg.sender] = safeSub(balances[msg.sender], safeMul(amountRequested, multiplier));
        balances[owner] = safeAdd(balances[owner], safeMul(amountRequested, multiplier));

        emit LogRefund(msg.sender, amountRequested);
        msg.sender.transfer(ethRequested);
        return true;
    }

    /* function refundInEth(uint256 amountRequested) external payable returns(bool) {
        require(amountRequested > 0,"requested quantity must be greater than 0");

        uint256 tokensRequested = curs(amountRequested);
        require(tokensRequested <= balances[msg.sender], "not enough balance in tokens");

        balances[msg.sender] = safeSub(balances[msg.sender], safeMul(tokensRequested, multiplier));
        balances[owner] = safeAdd(balances[owner], safeMul(tokensRequested, multiplier));

        emit LogRefund(msg.sender, tokensRequested);
        msg.sender.transfer(amountRequested);
        return true;
    } */

    /**
     * ERC 20 Standard Token interface transfer function
     *
     * Prevent transfers until halt period is over.
     */
    function transfer(address _to, uint256 _value) public isAvailable() returns (bool) {
        return super.transfer(_to, _value);
    }

    /**
     * ERC 20 Standard Token interface transfer function
     *
     * Prevent transfers until halt period is over.
     */
    function transferFrom(address _from, address _to, uint256 _value) public isAvailable() onlyOwner() returns (bool) {
        return super.transferFrom(_from, _to, _value);
    }

    modifier onlyOwner() {
        require(msg.sender == owner, "this function avalable only for owner");
        _;
    }

    modifier isAvailable() {
        require(!halted, "sale was stoped");
        _;
    }

    /**
     * Just being sent some cash? Let's buy tokens
     */
    function() external payable {
        buy();
    }

    /**
     * Emergency Stop ICO.
     */
    function halt() external onlyOwner() {
        halted = true;
    }

    function unHalt() external onlyOwner() {
        halted = false;
    }

    function getBalContract() external view onlyOwner() returns(uint256){
        return balContr;
    }
}
